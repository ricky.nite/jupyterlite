![Build Status](https://gitlab.com/ricky.nite/jupyterlite/badges/master/pipeline.svg)

# jupyterlite

Example JupyterLite site using GitLab Pages: https://ricky.nite.gitlab.io/jupyterlite

See: https://jupyterlite.readthedocs.io/en/latest/
